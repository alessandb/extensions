--[[
INSTALLATION (create directories if they don't exist):
- put the file in the VLC subdir /lua/extensions, by default:
* Windows (all users): %ProgramFiles%\VideoLAN\VLC\lua\extensions\
* Windows (current user): %APPDATA%\VLC\lua\extensions\
* Linux (all users): /usr/share/vlc/lua/extensions/
* Linux (current user): ~/.local/share/vlc/lua/extensions/
* Mac OS X (all users): /Applications/VLC.app/Contents/MacOS/share/lua/extensions/
- Restart VLC.

see https://www.videolan.org/developers/vlc/share/lua/README.txt for mods
]]--



INTIME = 0
OUTTIME = 0


--[[ Extension description ]]

function descriptor()
   return { 
      title = "CUTTER" ;
      version = "0.1" ;
      author = "yes" ;
      shortdesc = "Video Cutter";
      description = "<h1>Cutter</h1>"
      .. "it"
      .. "<br>cuts.";
      url = "https://github.com/what/ev"
   }
end

--[[ Hooks ]]

-- Activation hook
function activate()
   -- w = d:add_label( "My Label", 2, 3, 4, 5 ) will create a label at row 3, col 2, with a relative width of 4, height of 5.
   vlc.msg.info("[CUTTER] Activated")
   d = vlc.dialog("Cutter")
   d:add_button("IN", markin,1,1)
   d:add_button("OUT", markout,2,1)
   d:add_button("CUT", cutit,3,1)
   d:add_button("ALL", convertit,4,1)
   intext = d:add_text_input( "IN" )
   outtext = d:add_text_input( "OUT" )
   outputtext = d:add_text_input( "OUTPUT",1,9,99,4 )
   d:add_check_box( "hurr", true, 1,5 )
   d:add_check_box( "durr", true, 2,5 )
   d:add_button("EXEC", esegui)
   d:show()
   end

-- Deactivation hook
function deactivate()
   vlc.msg.dbg("[cutter Deactivated")
   vlc.deactivate()
end

function close()
   deactivate()
end

function esegui()
   vlc.msg.info("gnome-terminal -e '".. outputtext:get_text() .." ; sleep 60' ")
   os.execute("xterm -hold  -e '".. outputtext:get_text() .."' &")
   
end

function markin()
   -- boh
      vlc.msg.info("in")
      local input = vlc.object.input()
	  local elapsed_time = vlc.var.get(input, "time")
      vlc.msg.info(elapsed_time)
      intext:set_text(elapsed_time)
      INTIME=(elapsed_time)
end

function markout()
   -- boh
      local input = vlc.object.input()
	  local elapsed_time = vlc.var.get(input, "time")
      vlc.msg.info(elapsed_time)
      outtext:set_text(elapsed_time)
      OUTTIME=(elapsed_time)   
end

function cutit()
   -- boh
   
   local item
repeat
    item = vlc.input.item()
until (item and item:is_preparsed())

-- preparsing doesn't always provide all the information we want (like duration)
repeat
until item:stats()["demux_read_bytes"] > 0

vlc.msg.info("name: "..item:name())
vlc.msg.info("uri: "..vlc.strings.decode_uri(item:uri()))
vlc.msg.info("duration: "..tostring(item:duration()))

local meta = item:metas()
if meta then
    for key, value in pairs(meta) do
        vlc.msg.info("  "..key..": "..value)
    end
else
    vlc.msg.info("  no meta data available")
end
vlc.msg.info("info:")
for cat, data in pairs(item:info()) do
    vlc.msg.info("  "..cat)
    for key, value in pairs(data) do
        vlc.msg.info("    "..key..": "..value)
    end
end

outputtext:set_text(OUTTIME-INTIME)
item = vlc.input.item() -- get the current playing file
uri = item:uri() -- extract it's URI
filename = "/" .. vlc.strings.decode_uri(uri) -- decode %foo stuff from the URI
filename = string.sub(filename,9) -- remove 'file://' prefix which is 7 chars long

CMDLINE ="WTF???"
outputtext:set_text(CMDLINE)
preamble = "ffmpeg "
inopts = " "
outopts = " -acodec copy -vcodec copy "
filesub = string.gsub(filename, "...$","")
--filesub = filename
INTIME = intext:get_text()
OUTTIME = outtext:get_text()
len =  (OUTTIME-INTIME)
--outfile =  filesub .. string.gsub(INTIME,",",".") .. "-" .. string.gsub(OUTTIME,",",".") .. string.match(filename,"^.+(%..+)$")
outfile =  filesub .. string.gsub(INTIME,",",".") .. "-" .. string.gsub(OUTTIME,",",".") .. ".mp4"

vlc.msg.info(outfile)

CMDLINE = preamble .. " -ss " .. string.gsub(INTIME,",",".") .. " -t " .. string.gsub(len,",",".") .. " " .. inopts .. " -i \"" .. filename .. "\" " .. outopts .. " \""..outfile.."\" "
outputtext:set_text(CMDLINE)
vlc.msg.info(CMDLINE)
end

function convertit()
   -- boh
   
   local item
repeat
    item = vlc.input.item()
until (item and item:is_preparsed())

-- preparsing doesn't always provide all the information we want (like duration)
repeat
until item:stats()["demux_read_bytes"] > 0

vlc.msg.info("name: "..item:name())
vlc.msg.info("uri: "..vlc.strings.decode_uri(item:uri()))
vlc.msg.info("duration: "..tostring(item:duration()))

local meta = item:metas()
if meta then
    for key, value in pairs(meta) do
        vlc.msg.info("  "..key..": "..value)
    end
else
    vlc.msg.info("  no meta data available")
end
vlc.msg.info("info:")
for cat, data in pairs(item:info()) do
    vlc.msg.info("  "..cat)
    for key, value in pairs(data) do
        vlc.msg.info("    "..key..": "..value)
    end
end

outputtext:set_text(OUTTIME-INTIME)
item = vlc.input.item() -- get the current playing file
uri = item:uri() -- extract it's URI
filename = "/" .. vlc.strings.decode_uri(uri) -- decode %foo stuff from the URI
filename = string.sub(filename,9) -- remove 'file://' prefix which is 7 chars long

CMDLINE ="WTF???"
outputtext:set_text(CMDLINE)
preamble = "ffmpeg "
inopts = " "
outopts = " -acodec copy -vcodec copy "
filesub = string.gsub(filename, "...$","")
--filesub = filename
INTIME = 0
OUTTIME = item:duration()
len =  (OUTTIME-INTIME)
--outfile =  filesub .. string.gsub(INTIME,",",".") .. "-" .. string.gsub(OUTTIME,",",".") .. string.match(filename,"^.+(%..+)$")
outfile =  filesub .. string.gsub(INTIME,",",".") .. "-" .. string.gsub(OUTTIME,",",".") .. ".mp4"

vlc.msg.info(outfile)

CMDLINE = preamble .. " -ss " .. string.gsub(INTIME,",",".") .. " -t " .. string.gsub(len,",",".") .. " " .. inopts .. " -i '" .. filename .. "' " .. outopts .. " '"..outfile.."' "
outputtext:set_text(CMDLINE)
vlc.msg.info(CMDLINE)
end


--[[ The file deletion routine ]]
function delete()
   item = vlc.input.item() -- get the current playing file
   uri = item:uri() -- extract it's URI
   filename = "/" .. vlc.strings.decode_uri(uri) -- decode %foo stuff from the URI

   vlc.playlist.skip(1)
   
   --[[ 
   --vlc.misc.mwait(vlc.misc.mdate() + 3000000)
   --vlc.var.add_callback(vlc.object.input(), "time", vlc.playlist.skip(1), 3000000)
   ]]--
   
   --retval, err = os.chmod(filename, 0777)
   filename = string.sub(filename,9) -- remove 'file://' prefix which is 7 chars long
   vlc.msg.info("#[Diskdelete] selected for deletion: " .. filename)
   vlc.msg.info("ls " .. filename .. " # run later?")
   vlc.msg.info("rm " .. filename .. " # run later?")
   
   
   retval, err = os.remove(filename) -- delete the file with this filename from disk
   if(retval == nil) -- error handling; if deletion failed, print why
   then
   vlc.msg.info("[Diskdelete] error: " .. err)

end

end

-- This empty function is there, because vlc pested me otherwise
function meta_changed()
end
